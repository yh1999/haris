package id.bootcamp.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import id.bootcamp.repositories.MusicRepository;
import id.bootcamp.services.MusicService;
import id.bootcamp.services.MusicServiceImplements;

@Configuration
public class IocBeanConfig {
	
	@Bean
	MusicService musicService(MusicRepository mr) {
		return new MusicServiceImplements(mr);
	}
//	
//	@Bean
//	SingerService singerService(SingerRepository sr) {
//		return new SingerServiceImplements(sr);
//	}
}
