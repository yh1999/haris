package id.bootcamp.controllers;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.bootcamp.models.MusicModel;
import id.bootcamp.services.MusicService;

@RestController
@RequestMapping("musicAPI")
public class MusicRestController {

	private MusicService ms;

	public MusicRestController(MusicService ms) {
		this.ms = ms;
	}

	@GetMapping("getAllMusic")
	public List<MusicModel> getAllMusic() {
		List<MusicModel> musics = ms.getAllMusic();
		List<MusicModel> sortedMusic = musics.stream()// .sorted((id1, id2) -> id1.getId().compareTo(id2.getId()))
				.sorted((o1, o2) -> o1.getName().compareTo(o2.getName())).collect(Collectors.toList());
		return sortedMusic;
	}

	@GetMapping("searchMusic")
	public List<MusicModel> searchMusic(@RequestParam String search) {
		List<MusicModel> musics = ms.getAllMusic().stream()// .sorted((id1, id2) -> id1.getId().compareTo(id2.getId()))
				.sorted((o1, o2) -> o1.getName().compareTo(o2.getName())).collect(Collectors.toList());
		List<MusicModel> result = musics.stream().filter(m -> m.getName().toLowerCase().contains(search.toLowerCase()))
				.collect(Collectors.toList());
		return result;
	}

	@GetMapping("getMusicById/{id}")
	public MusicModel getMusic(@PathVariable("id") Long id) {
		return ms.getMusicById(id);
	}

	@PostMapping("insertMusic")
	public String insertMusic(@RequestBody MusicModel mm) {
		mm.setCreateDate(new Date());
		MusicModel result = ms.insertMusic(mm);
		System.out.println(result.getId());

		return "ok";
	}

	@PutMapping("editMusic")
	public String editMusic(@RequestBody MusicModel mm) {
		MusicModel mmBefore = ms.getMusicById(mm.getId());
		mm.setCreateDate(mmBefore.getCreateDate());
		mm.setModifyDate(new Date());
		ms.editMusic(mm);

		return "ok";
	}

	@DeleteMapping("deleteMusic/{id}")
	public String deleteMusic(@PathVariable("id") Long id) {
		ms.deleteMusic(id);
		return "ok";
	}
}
