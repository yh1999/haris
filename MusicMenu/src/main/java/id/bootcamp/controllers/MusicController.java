package id.bootcamp.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MusicController {

	@RequestMapping("")
	public String getMusicView() {
		return "dashboard.html";
	}
}
