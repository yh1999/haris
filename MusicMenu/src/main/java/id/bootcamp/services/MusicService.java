package id.bootcamp.services;

import java.util.List;


import id.bootcamp.models.MusicModel;

public interface MusicService {
	
	public List<MusicModel> getAllMusic();

	public List<MusicModel> getMusicBySingerId(Long id);

	public MusicModel getMusicById(Long id);

	public MusicModel insertMusic(MusicModel mm);

	public void editMusic(MusicModel mm);

	public void deleteMusic(Long id);
}
