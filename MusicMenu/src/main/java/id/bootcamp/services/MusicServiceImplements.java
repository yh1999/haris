package id.bootcamp.services;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.bootcamp.models.MusicModel;
import id.bootcamp.repositories.MusicRepository;

@Transactional
public class MusicServiceImplements implements MusicService {

	private MusicRepository mr;

	public MusicServiceImplements(MusicRepository mr) {
		this.mr = mr;
	}

	public List<MusicModel> getMusicBySingerId(Long id) {
		return mr.getMusicById(id);
	}

	
	public List<MusicModel> getAllMusic() {
		return mr.findAll();
	}

	
	public MusicModel getMusicById(Long id) {
		return mr.findById(id).get();
	}

	
	public MusicModel insertMusic(MusicModel mm) {
		return mr.save(mm);
	}

	
	public void editMusic(MusicModel mm) {
		mr.save(mm);
	}

	
	public void deleteMusic(Long id) {
		mr.deleteById(id);
	}

}
