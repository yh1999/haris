package id.bootcamp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import id.bootcamp.models.MusicModel;

public interface MusicRepository extends JpaRepository<MusicModel, Long> {

	@Query(value = "SELECT * FROM music WHERE singer_id = :id", nativeQuery = true)
	List<MusicModel> getMusicById(Long id);
}
