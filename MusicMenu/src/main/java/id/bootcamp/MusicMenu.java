package id.bootcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MusicMenu {

	public static void main(String[] args) {
		SpringApplication.run(MusicMenu.class, args);
	}

}
